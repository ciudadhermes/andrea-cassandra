export default {
    buildModules: [
        '@nuxt/typescript-build',
        '@nuxtjs/tailwindcss'
    ],
    css: [
        '~/assets/css/main.scss'
    ]
}
